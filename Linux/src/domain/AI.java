package src.domain;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class AI {
	
	private Neuron[] neurons;



	//CONSTRUCTOR//
	public AI(int nbNeuron, int nbEntries) {
		
		System.out.println("TRY : Init AI & neurons");
		
		//Init each neurons
		this.neurons = new Neuron[nbNeuron];
		for(int n=0; n < nbNeuron; n++) {
			this.neurons[n] = new Neuron(nbEntries);
		}
		
		System.out.println("INIT : AI & " + this.neurons.length + " neurons");
	}
	
	
	
	//METHODS
	
	//LEARNING//
	/*Learn to the neuron's from inputs gets*/
	public void learning(float[][] output_fft, JLabel displayNetwork, BufferedImage image) {
		for(int n=0; n < this.neurons.length; n++){//For each neurons
			
			//2.1) Init wanted result
			float[] wantedResults = new float[this.neurons.length];
			for(int i=0; i<this.neurons.length; i++) {
				wantedResults[i] = -1;
			}
			wantedResults[n] = 1;

			//2.2) Learn neurons
			System.out.println("TRY : Learning neuron " + n);
			this.neurons[n].learning(
				output_fft,
				wantedResults
			);
			System.out.println("LEARN : Neuron " + n + "\n");
			
			//2.3) Load neuron animation
			this.reloadNeuron(n, true, displayNetwork, image);
			this.reloadOutput(n, true, displayNetwork, image);
		}
		
		System.out.println("\nLEARNING DONE");
		System.out.println("GOT : " + this.neurons.length + " neuron(s) learned");
	}
	
	
	
	//RELOAD NEURON//
	/*Reload each neuron according to the output value*/
	public void reloadNeuron(int numNeuron, boolean isActive, JLabel displayNetwork, BufferedImage image) {
		
		//1) Get origin point & color
		int x0 = image.getWidth()/2;
		int y0 = image.getHeight()/(this.neurons.length+1);
		int size = y0/4;
		Color color;
		color = isActive ? Color.GREEN : Color.RED;
		
		//2) Draw each neuron
		for(int x=x0-size; x<x0+size; x++) {
			for(int y=(numNeuron+1)*y0-size; y<(numNeuron+1)*y0+size; y++) {
				if(Math.sqrt(Math.pow(y-y0*(numNeuron+1), 2) + Math.pow(x-x0, 2)) < size){
					image.setRGB(x, y, color.getRGB());
				}
			}
		}
		
		//3) Set & display image
		ImageIcon newImg = new ImageIcon(image);
		displayNetwork.setIcon(newImg);
	}
	
	
	
	
	//RELOAD OUTPUT//
	/*Reload each output according to the output value*/
	public void reloadOutput(int numOutput, boolean isActive, JLabel displayNetwork, BufferedImage image) {
		
		//1) Get origin point & color
		int x0Output = (image.getWidth()/6)*5;
		int y0 = image.getHeight()/(this.neurons.length+1);
		int size = y0/4;
		Color color;
		color = isActive ? Color.GREEN : Color.RED;
		
		//2) Draw each output
		for(int x=x0Output-size; x<x0Output+size; x++) {
			for(int y=(numOutput+1)*y0-size; y<(numOutput+1)*y0+size; y++) {
				if(Math.sqrt(Math.pow(y-y0*(numOutput+1), 2) + Math.pow(x-x0Output, 2)) < size){
					image.setRGB(x, y, color.getRGB());
				}
			}
		}
		
		//3) Set & display image
		ImageIcon newImg = new ImageIcon(image);
		displayNetwork.setIcon(newImg);
	}

	

	//SETTER & GETTER//
	public Neuron[] getNeurons() {
		return neurons;
	}

	public void setNeurons(Neuron[] neurons) {
		this.neurons = neurons;
	}
}
