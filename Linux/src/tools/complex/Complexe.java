package src.tools.complex;

public interface Complexe {
	
	public double reel();
	public double imag();
	public double mod();
	public double arg();

	//OPERATION//
	public Complexe add(final Complexe complexe);
	public Complexe sub(final Complexe complexe);
	public Complexe mul(final Complexe complexe);
	public Complexe plus(final Complexe complexe);
	public Complexe moins(final Complexe complexe);
	public Complexe fois(final Complexe complexe);
}
