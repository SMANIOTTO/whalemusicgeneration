::clear all compilation results
del src\domain\*.class src\tools\*.class src\tools\complex\*.class src\tools\wav\*.class src\ui\*.class src\whaleMusicGeneration.class



::compile all
jdk-14.0.1\bin\javac src\domain\*.java src\tools\*.java src\tools\complex\*.java src\tools\wav\*.java src\ui\*.java src\whaleMusicGeneration.java



::run
jdk-14.0.1\bin\java src/whaleMusicGeneration
