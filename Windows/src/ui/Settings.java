package src.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import src.tools.Picture;

import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class Settings extends JFrame {

	//AREA
	private JTextField neuronNumber;
	private JTextField nameSoundText;
	
	//TOOLS
	private Picture tools = new Picture();
	

	
	//CREATE PANE//
	/*Create settings pane menu*/
	public JPanel createPane(JButton btnBackSettings) {
		
		//1) Create pane
		JPanel newPane = new JPanel();
		newPane.setBackground(new Color(34, 51, 59));
		newPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		newPane.setLayout(null);
		
		//2) Footer label
		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon("resources/ui/footer.png"));
		footer.setBounds(0, 628, 1264, 53);
		newPane.add(footer);
		
		//3) Display
		JPanel display = new JPanel();
		display.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		display.setBackground(Color.BLACK);
		display.setBounds(666, 30, 567, 567);
		newPane.add(display);
		
		//3.1) Drawing label
		display.setBackground(Color.BLACK);
		display.setLayout(new BorderLayout(0, 0));
		JLabel imageSettings = new JLabel("");
		ImageIcon settingsImage = tools.resize("resources/ui/settings.png", 500, 500);
		imageSettings.setIcon(settingsImage);
		imageSettings.setHorizontalAlignment(SwingConstants.CENTER);
		imageSettings.setBackground(Color.BLACK);
		display.add(imageSettings, BorderLayout.CENTER);
		
		//4) Desc
		JPanel desc = new JPanel();
		desc.setBounds(29, 193, 640, 336);
		desc.setBackground(Color.WHITE);
		newPane.add(desc);
		desc.setLayout(null);
		
		//4.1) Title 2
		JLabel labelName = new JLabel("Sound name");
		labelName.setHorizontalAlignment(SwingConstants.CENTER);
		labelName.setFont(new Font("Arial", Font.PLAIN, 20));
		labelName.setBounds(109, 100, 143, 33);
		desc.add(labelName);
		
		//4.2) TextArea browse
		nameSoundText = new JTextField();
		nameSoundText.setBounds(288, 110, 209, 20);
		desc.add(nameSoundText);
		nameSoundText.setColumns(10);
		
		//4.4) Title 3
		JLabel labelNbNeuron = new JLabel("Neuron number");
		labelNbNeuron.setHorizontalAlignment(SwingConstants.CENTER);
		labelNbNeuron.setFont(new Font("Arial", Font.PLAIN, 20));
		labelNbNeuron.setBounds(109, 210, 143, 33);
		desc.add(labelNbNeuron);
		
		//4.5) TextArea nb Neurons
		neuronNumber = new JTextField();
		neuronNumber.setBounds(288, 217, 209, 20);
		desc.add(neuronNumber);
		neuronNumber.setColumns(1);
		
		//5) Desc right
		JPanel descRight = new JPanel();
		descRight.setBounds(1231, 193, 23, 336);
		newPane.add(descRight);
		
		//6) Controls
		JPanel controls = new JPanel();
		controls.setBackground(new Color(34, 51, 59));
		controls.setBounds(29, 527, 640, 90);
		newPane.add(controls);
		controls.setLayout(null);
		
		//7) Btns
		controls.add(btnBackSettings);	//BACK SETTINGS
		
		//8) Menu title
		JLabel menuTitle = new JLabel("SETTINGS");
		menuTitle.setHorizontalAlignment(SwingConstants.RIGHT);
		menuTitle.setForeground(Color.WHITE);
		menuTitle.setFont(new Font("Arial", Font.PLAIN, 70));
		menuTitle.setBounds(101, 124, 556, 65);
		newPane.add(menuTitle);
		
		return newPane;
	}



	//GETTER//
	public int getNbNeurons() {
		return neuronNumber.getText().length() == 0 ? 12 : Integer.parseInt(neuronNumber.getText());
	}

	public String getFileName() {
		return nameSoundText.getText().length() == 0 ? "default" : nameSoundText.getText();
	}	
}
