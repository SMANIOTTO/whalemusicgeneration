package src.ui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import src.domain.Sound;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JButton;
import java.io.File;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class Generation extends JFrame {

	//SOUND
	private File audioFile;
	private Clip clip;
	
	//DISPLAY
	private JLabel displaySound = new JLabel("");
	private JLabel sampleRateRes = new JLabel("Hz");
	private JLabel durationRes = new JLabel("00:00");
	private JLabel samplesRes = new JLabel("x");
	private JLabel musicName = new JLabel("File name");
	
	
	
	//CREATE PANE//
	/*Create generation pane menu*/
	public JPanel createPane(JButton btnBackGeneration, JButton btnReload) {
		
		//1) Create pane
		JPanel newPane = new JPanel();
		newPane.setBackground(new Color(34, 51, 59));
		newPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		newPane.setLayout(null);
		
		//2) Footer label
		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon("resources/ui/footer.png"));
		footer.setBounds(0, 628, 1264, 53);
		newPane.add(footer);
		
		//3) Display
		JPanel display = new JPanel();
		display.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		display.setBackground(Color.BLACK);
		display.setBounds(666, 30, 567, 567);
		newPane.add(display);
		
		//3.1) Drawing label
		display.setBackground(Color.BLACK);
		display.setLayout(null);
		displaySound.addMouseListener(new MouseAdapter() {

			//3.1.1) Display hand cursor
			public void mouseEntered(MouseEvent arg0) {
				if(audioFile != null) {
					Cursor newCursor = new Cursor(Cursor.HAND_CURSOR);
					displaySound.setCursor(newCursor);
				}
			}
			
			//3.1.2) Stop previous sound and play new
			public void mouseClicked(MouseEvent e) {
				if(audioFile != null) {
					try {
						
						//3.1.2.1) Stop sound
						if(clip != null) {
							clip.stop();
							clip.close();
						}
						
						//3.1.2.2) Start sound
				        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile.toURI().toURL());
				        clip = AudioSystem.getClip();
				        clip.open(audioInputStream);
				        clip.start();
				    } 
					catch (Exception ex) {
				        ex.printStackTrace();
				    }
				}
			}
		});
		displaySound.setBounds(2, 33, 563, 466);
		displaySound.setBackground(Color.BLACK);
		display.add(displaySound);
		
		//4) Info pane
		JPanel infoPane = new JPanel();
		infoPane.setBackground(Color.RED);
		infoPane.setBounds(2, 499, 563, 68);
		display.add(infoPane);
		infoPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		//5) Name pane
		JPanel namePane = new JPanel();
		namePane.setBackground(Color.BLACK);
		infoPane.add(namePane);
		namePane.setLayout(new GridLayout(0, 3, 0, 0));
		
		//5.1) Sample rate
		JLabel sampleRate = new JLabel("Sample rate");
		sampleRate.setFont(new Font("Arial", Font.PLAIN, 17));
		sampleRate.setHorizontalAlignment(SwingConstants.CENTER);
		sampleRate.setForeground(Color.WHITE);
		namePane.add(sampleRate);
		
		//5.2) Duration
		JLabel duration = new JLabel("Duration");
		duration.setHorizontalAlignment(SwingConstants.CENTER);
		duration.setForeground(Color.WHITE);
		duration.setFont(new Font("Arial", Font.PLAIN, 17));
		namePane.add(duration);
		
		//5.3) Samples
		JLabel sample = new JLabel("Samples");
		sample.setHorizontalAlignment(SwingConstants.CENTER);
		sample.setForeground(Color.WHITE);
		sample.setFont(new Font("Arial", Font.PLAIN, 17));
		namePane.add(sample);
		
		//5.4) Info display pane
		JPanel infoDisplayPane = new JPanel();
		infoPane.add(infoDisplayPane);
		infoDisplayPane.setLayout(new GridLayout(0, 3, 0, 0));
		
		//5.5) Sample rate res
		sampleRateRes.setHorizontalAlignment(SwingConstants.CENTER);
		sampleRateRes.setForeground(Color.BLACK);
		sampleRateRes.setFont(new Font("Arial", Font.PLAIN, 17));
		infoDisplayPane.add(sampleRateRes);
		
		//5.6) Duration res
		durationRes.setHorizontalAlignment(SwingConstants.CENTER);
		durationRes.setFont(new Font("Arial", Font.PLAIN, 17));
		durationRes.setForeground(Color.BLACK);
		infoDisplayPane.add(durationRes);
		
		//5.7) Samples res
		samplesRes.setHorizontalAlignment(SwingConstants.CENTER);
		samplesRes.setForeground(Color.BLACK);
		samplesRes.setFont(new Font("Arial", Font.PLAIN, 17));
		infoDisplayPane.add(samplesRes);
		
		//6) Desc
		JPanel desc = new JPanel();
		desc.setBounds(29, 193, 640, 336);
		desc.setBackground(Color.WHITE);
		newPane.add(desc);
		desc.setLayout(new BorderLayout(0, 0));
		
		//6.1) Music name
		musicName.setForeground(Color.BLACK);
		musicName.setHorizontalAlignment(SwingConstants.CENTER);
		musicName.setFont(new Font("Arial", Font.PLAIN, 30));
		desc.add(musicName, BorderLayout.CENTER);
		
		//7) Desc right
		JPanel descRight = new JPanel();
		descRight.setBounds(1231, 193, 23, 336);
		newPane.add(descRight);
		
		//8) Controls
		JPanel controls = new JPanel();
		controls.setBackground(new Color(34, 51, 59));
		controls.setBounds(29, 527, 640, 90);
		newPane.add(controls);
		controls.setLayout(null);
		
		//9) Btns
		controls.add(btnBackGeneration);	//BACK GENERATION
		controls.add(btnReload);			//RELOAD
		
		//10) Menu title
		JLabel menuTitle = new JLabel("GENERATION");
		menuTitle.setHorizontalAlignment(SwingConstants.RIGHT);
		menuTitle.setForeground(Color.WHITE);
		menuTitle.setFont(new Font("Arial", Font.PLAIN, 70));
		menuTitle.setBounds(101, 124, 556, 65);
		newPane.add(menuTitle);
		
		return newPane;
	}
	
	
	
	//LOAD AUDIO FILE//
	/*Display sound file frequency and informations*/
	public void loadAudioFile(File file) {
		
		//1) Create sound
		Sound soundEntry = new Sound(file.getAbsolutePath());

		//2) Treat the sound
		if(soundEntry != null && soundEntry.getDatas() != null){
			audioFile = file;
			
			//2.1) Create image
			BufferedImage image = new BufferedImage(567, 567, BufferedImage.TYPE_INT_ARGB);
			setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
			int imgLimit = image.getWidth()-1;
			
			//2.2) Display audio data
			sampleRateRes.setText(soundEntry.getFrequency() + "Hz");//SAMPLE RATE
			int s = soundEntry.getDatas().length/soundEntry.getFrequency();
			int m = 0;
			while(s > 59) {
				s-=60;
				m++;
			}
			String ss = s < 10 ? "0" + s : String.valueOf(s);
			String ms = m < 10 ? "0" + m : String.valueOf(m);
			durationRes.setText(ms + ':' + ss);//DURATION
			samplesRes.setText(String.valueOf(soundEntry.getDatas().length));//SAMPLES
			musicName.setText("resources/soundGenerated/" + file.getName().split("\\.")[0] + ".wav");
			
			//2.3) Create new list of img size
			Float[] entries = new Float[image.getWidth()];
			int step = (int) Math.ceil((double)(soundEntry.getDatas().length/(double)imgLimit));
			//System.out.println("datas nb = " + soundEntry.getDatas().length + " step = " + (int) Math.ceil((double)(soundEntry.getDatas().length/(double)(image.getWidth()-1))));
			for(int i=0; i<soundEntry.getDatas().length; i+=step) {
				entries[i/step] = soundEntry.getDatas()[i];
			}
			
			//2.4) Get extrem value
			float maxValue = 0;
			float minValue = 0;
			for(int i=0; i<imgLimit; i++) {
				maxValue = entries[i] > maxValue ? entries[i] : maxValue;
				minValue = entries[i] < minValue ? entries[i] : minValue;
			}
			
			//System.out.println("min = " + minValue + "max = " + maxValue);
			
			//2.5) Construct image
			for(int x=0; x<imgLimit; x++) {
				float valueToReach = 0;
				
				//2.5.1) Positive amp
				if(entries[x] > 0) {
					valueToReach = (image.getHeight()/2) - (entries[x]*(567/2))/maxValue;
					for(int y=image.getHeight()/2; y>valueToReach-1; y--) {
						image.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
				
				//2.5.2) Negative amp
				else if(entries[x] < 0){
					valueToReach = (image.getHeight()/2) + (entries[x]*(567/2))/minValue;
					for(int y=image.getHeight()/2; y<valueToReach-1; y++) {
						image.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
			
			//3) Set & display image
			ImageIcon newImg = new ImageIcon(image);
			displaySound.setIcon(newImg);
		}
		else {//ERROR
			System.out.println("ERROR : Getting datas from audio file");
		}
	}
	
	

}
