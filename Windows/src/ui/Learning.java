package src.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import src.domain.AI;
import src.domain.Sound;
import src.tools.Signal;

import javax.swing.JButton;
import java.awt.image.BufferedImage;
import java.awt.Font;
import javax.swing.SwingConstants;

public class Learning extends JFrame {
	
	//IMAGE
	private BufferedImage image;
	
	//DISPLAY
	private int nbNeuron;
	private JLabel displayNetwork = new JLabel("");
	
	
	
	//CREATE PANE//
	/*Create learning pane menu*/
	public JPanel createPane(int nbNeuron, JButton btnBackLearning) {
		this.nbNeuron = nbNeuron;
		
		//1) Create pane
		JPanel newPane = new JPanel();
		newPane.setBackground(new Color(34, 51, 59));
		newPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		newPane.setLayout(null);
		
		//2) Footer
		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon("resources/ui/footer.png"));
		footer.setBounds(0, 628, 1264, 53);
		newPane.add(footer);
		
		//3) Display
		JPanel display = new JPanel();
		display.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		display.setBackground(Color.BLACK);
		display.setBounds(666, 30, 567, 567);
		newPane.add(display);
		
		//3.1) Drawing label
		display.setBackground(Color.BLACK);
		display.setLayout(null);
		displayNetwork.setBounds(2, 33, 563, 466);
		displayNetwork.setBackground(Color.BLACK);
		loadNeuralNetwork(12);
		display.add(displayNetwork);
		
		//3.2) Message event
		JLabel messageEvent = new JLabel("Loading message ...");
		messageEvent.setBounds(2, 538, 563, 29);
		display.add(messageEvent);
		messageEvent.setHorizontalAlignment(SwingConstants.CENTER);
		messageEvent.setFont(new Font("Arial", Font.PLAIN, 20));
		messageEvent.setForeground(Color.WHITE);
		
		//4) Desc
		JPanel desc = new JPanel();
		desc.setBounds(29, 193, 640, 336);
		desc.setBackground(Color.WHITE);
		newPane.add(desc);
		desc.setLayout(new BorderLayout(0, 0));
		
		//4.1) Desc title
		JLabel loadingTitle = new JLabel("Loading ...");
		loadingTitle.setHorizontalAlignment(SwingConstants.CENTER);
		loadingTitle.setFont(new Font("Arial", Font.PLAIN, 30));
		desc.add(loadingTitle, BorderLayout.SOUTH);
		
		//5) Desc right
		JPanel descRight = new JPanel();
		descRight.setBounds(1231, 193, 23, 336);
		newPane.add(descRight);
		
		//6) Controls
		JPanel controls = new JPanel();
		controls.setBackground(new Color(34, 51, 59));
		controls.setBounds(29, 527, 640, 90);
		newPane.add(controls);
		controls.setLayout(null);
		
		//7) Btns
		controls.add(btnBackLearning);	//BACK LEARNING
		
		//7) Menu title
		JLabel menuTitle = new JLabel("LEARNING");
		menuTitle.setHorizontalAlignment(SwingConstants.RIGHT);
		menuTitle.setForeground(Color.WHITE);
		menuTitle.setFont(new Font("Arial", Font.PLAIN, 70));
		menuTitle.setBounds(101, 124, 556, 65);
		newPane.add(menuTitle);
		
		return newPane;
	}
	

	
	//LOAD NEURAL NETWORK//
	/*Load neural network on screen according to nb neuron*/
	private void loadNeuralNetwork(int nbNeuron) {
		
		//1) Create image
		image = new BufferedImage(567, 467, BufferedImage.TYPE_INT_ARGB);
		setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		
		//2) Display entry
		int sizeEntry = 50;
		int x0Entry = 65;
		int y0Entry = image.getHeight()/2;
		for(int x=x0Entry-sizeEntry; x<x0Entry+sizeEntry; x++) {
			for(int y=y0Entry-sizeEntry; y<y0Entry+sizeEntry; y++) {
				if(Math.sqrt(Math.pow(y-y0Entry, 2) + Math.pow(x-x0Entry, 2)) < sizeEntry){
					image.setRGB(x, y, Color.WHITE.getRGB());
				}
			}
		}
		
		//3) Display neuron & line
		for(int i=1; i<nbNeuron+1; i++) {
			
			//3.1) Get origin point
			int x0 = image.getWidth()/2;
			int y0 = image.getHeight()/(nbNeuron+1);
			int size = y0/4;
			
			//3.2) Draw each neuron
			for(int x=x0-size; x<x0+size; x++) {
				for(int y=i*y0-size; y<i*y0+size; y++) {
					if(Math.sqrt(Math.pow(y-y0*i, 2) + Math.pow(x-x0, 2)) < size){
						image.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
			
			//3.3) draw each line
			int xLine = x0Entry;
			int yLine = y0Entry;
			while(Math.sqrt(Math.pow(i*y0-yLine, 2) + Math.pow((image.getWidth()/6)*5-xLine, 2)) > 0) {
				if(xLine < (image.getWidth()/6)*5) {
					xLine++;
				}
				
				if(yLine < i*y0) {
					yLine++;
				}
				else if(yLine > i*y0) {
					yLine--;
				}
				image.setRGB(xLine, yLine, Color.WHITE.getRGB());
			}
				
		
			//3.4) Draw each output
			int x0Output = (image.getWidth()/6)*5;
			for(int x=x0Output-size; x<x0Output+size; x++) {
				for(int y=i*y0-size; y<i*y0+size; y++) {
					if(Math.sqrt(Math.pow(y-y0*i, 2) + Math.pow(x-x0Output, 2)) < size){
						image.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
		}
		
		//4) Set & display image
		ImageIcon newImg = new ImageIcon(image);
		displayNetwork.setIcon(newImg);
	}
	
	
	
	//LAUNCH//
	public void start(int nbNeurons, int fftLength, AI ai) {
		Signal signal = new Signal();
		
		this.loadNeuralNetwork(nbNeurons);
		
		//2) Load default tones (useful for sound generation)
		float[][] outputFFT = signal.getFFTSamplesSignal(nbNeurons, fftLength);
		
		System.out.println("\n\n// LEARNING //");
		ai.learning(outputFFT, displayNetwork, image);
	}
}
