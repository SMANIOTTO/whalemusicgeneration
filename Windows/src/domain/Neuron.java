package src.domain;

public class Neuron
{
	public float state = 0.15f;//State
	public float threshold = 1.e-2f;//Threshold
	private float[] synapses;//weights & bias(last)
	private float output = Float.NaN;
	

	
	//CONSTRUCTOR//
	public Neuron(final int nbEntrees){
		
		//1) Add bias (last entry+1)
		this.synapses = new float[nbEntrees+1];
		
		//2) Set random weights & bias
		for(int i = 0; i < nbEntrees+1; ++i)
			this.synapses[i] = (float)(Math.random()*2.-1.);
	}
	
	
	
	//METHODS//
	
	//TRANSFER FUNCTION//
	/*Function defining if threshold pass or not*/
	public float transferFunction(final float valeur) {
		return valeur >= 0 ? 1.f : -1.f;
	}



	//UPDATE//
	/*Calculate outputs according to weights, bias and transfer function*/
	public void update(final float[] entrees){
		
		//1) Get bias
		float somme = this.synapses[this.synapses.length-1];
		
		//2) Calculate weighted sum
		for (int i = 0; i < this.synapses.length-1; ++i)
			somme += entrees[i]*this.synapses[i];
		
		//3) Get correct neuron or not
		output = transferFunction(somme);
	}



	//LEARNING//
	/*Update weights & bias according to data wanted*/
	public void learning(final float[][] entrees, final float[] resultats) {
		boolean apprentissageFini = false;

		while(!apprentissageFini) {	
	
			for(int i=0; i<entrees.length; i++) {
				apprentissageFini = isSame(entrees[i], resultats[i]);

				if(!apprentissageFini) {
					learn(entrees[i], resultats[i]);
					System.out.println("GOT : Wrong result input " + i + " = Restarting");
					break;
				}
			}
			
		}
	}
	//IS SAME//
	/*Check if input same as output*/
	public boolean isSame(float[] entrees, float resultat) {
		//1) Get output
		update(entrees);
		
		//2) test if the same
		return (output == resultat);
	}
	//LEARN//
	/*Learn neurons's weights and bias from input and results wanted*/
	private void learn(float[] entrees, float resultat) {
		//1) Calculate new weights
		for(int j=0; j < getSynapses().length-1; j++) {//For each weights
			this.synapses[j] += entrees[j] * this.state * (resultat - this.output);//Weight formula
		}
		
		//2) Calculate new bias
		this.synapses[this.synapses.length-1] += this.state * (resultat - this.output);//Bias formula
	}



	//SETTER & GETTER//
	public float[] getSynapses() {
		return synapses;
	}

	public float getOutput() {
		return output;
	}
}
