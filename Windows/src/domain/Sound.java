package src.domain;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteOrder;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioFormat;

//FILE REQUIRED : WAV 16bits PCM signed mono//

public class Sound {
	
	final public int sampleChunk = 220500;
	private int frequency;
	private float[] datas;
	
	

	//CONSTRUCTOR//
	/*Creating sound from file*/
	public Sound(final String nomFichier) {
		try	{
			//1) Open audio file
			AudioInputStream ais = AudioSystem.getAudioInputStream(new File(nomFichier));
			AudioFormat af = ais.getFormat();

			//Signal mono, PCM signed, 16 bits
			if (af.getChannels() == 1 && af.getEncoding() == AudioFormat.Encoding.PCM_SIGNED &&	af.getSampleSizeInBits() == 16)	{
				
				//1.1) Create buffer
				final int NombreDonnees = ais.available();//Bytes composed by datas
				final byte[] bufferOctets = new byte[NombreDonnees];
				
				//1.2) Read buffer and close file
				ais.read(bufferOctets);
				ais.close();
				
				//1.3) Prepare reading file
				ByteBuffer bb = ByteBuffer.wrap(bufferOctets);//Prepare buffer
				bb.order(ByteOrder.LITTLE_ENDIAN);//Set datas format to get
				ShortBuffer donneesAudio = bb.asShortBuffer();//Create buffer to read data
				
				//1.4) Get datas from file
				datas = new float[donneesAudio.capacity()];
				for (int i = 0; i < datas.length; ++i) {
					datas[i] = (float)donneesAudio.get(i);
				}
				frequency = (int)af.getSampleRate();//Get frequency from file
			}else
				System.out.println("FORMAT ERROR");
			
		}
		catch (Exception e) {//ERROR
			e.printStackTrace();
		}
	}



	//SETTER & GETTER//	
	public int getFrequency() {
		return frequency;
	}

	public float[] getDatas() {
		return datas;
	}



	//MAIN TEST//
	public static void main (String[] args)
	{
		if (args.length == 1)
		{
			System.out.println("Lecture du fichier WAV "+args[0]);
			Sound son = new Sound(args[0]);
			System.out.println("Fichier "+args[0]+" : "+son.getDatas().length+" échantillons à "+son.getFrequency()+"Hz");
		}
		else {
			System.out.println("Veuillez donner le nom d'un fichier WAV en paramètre SVP.");
		}
	}
}
