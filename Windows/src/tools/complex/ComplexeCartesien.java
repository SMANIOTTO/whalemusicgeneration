package src.tools.complex;

public class ComplexeCartesien implements Complexe
{
	private double reel;
	private double imag;

	
	
	//CONSTRUCTOR//
	public ComplexeCartesien(double reel, double imag)
	{
		this.reel = reel;
		this.imag = imag;
	}

	
	
	//OVERRIDE//
	public double reel() {
		return reel;
	}
	public double imag() {
		return imag;
	}
	
	
	
	//METHODS//
	public double mod() {
		return Math.sqrt(reel*reel+imag*imag);
	}
	public double arg() {
		return Math.atan2(imag, reel);
	}

	public Complexe add(final Complexe complexe)
	{
		this.reel += complexe.reel();
		this.imag += complexe.imag();
		return this;
	}

	public Complexe sub(final Complexe complexe)
	{
		this.reel -= complexe.reel();
		this.imag -= complexe.imag();
		return this;
	}

	public Complexe mul(final Complexe complexe)
	{
		double rtemp = reel*complexe.reel()-imag*complexe.imag();
		imag = reel*complexe.imag()+imag*complexe.reel();
		reel = rtemp;
		return this;
	}

	public Complexe plus(final Complexe complexe)
	{
		return new ComplexeCartesien(reel(), imag()).add(complexe);
	}

	public Complexe moins(final Complexe complexe)
	{
		return new ComplexeCartesien(reel(), imag()).sub(complexe);
	}

	public Complexe fois(final Complexe complexe)
	{
		return new ComplexeCartesien(reel(), imag()).mul(complexe);
	}



	//SETTER & GETTER//
	public double getReel() {
		return reel;
	}

	public void setReel(double reel) {
		this.reel = reel;
	}

	public double getImag() {
		return imag;
	}

	public void setImag(double imag) {
		this.imag = imag;
	}
}
