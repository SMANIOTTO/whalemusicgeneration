This project was made for ISEN Toulon, an engineer school :
                        www.isen.fr



WhaleMusicGeneration is an Artificial Intelligence that creates
a whale sound corresponding to the audio given as entry.



This project exists in 3 versions : Linux, Windows and Eclipse.
Just take a look at the README.txt file in each version.



By :
                                         GitLab                               GitHub
    - Gabriel SMANIOTTO        : https://gitlab.com/SMANIOTTO
    - Sebastien SILVANO (I.A.) : https://gitlab.com/iasebsil83        https://github.com/iasebsil83
    - Frederic TERRASSON       : https://gitlab.com/terminator9896    https://github.com/terminator9896
    - Axel MELONI (eerrtrr)    : https://gitlab.com/eerrtrr           https://github.com/eerrtrr

