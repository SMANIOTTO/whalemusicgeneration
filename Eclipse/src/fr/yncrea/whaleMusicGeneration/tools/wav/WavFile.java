package fr.yncrea.whaleMusicGeneration.tools.wav;

import java.io.*;

import fr.yncrea.whaleMusicGeneration.tools.Signal;

public class WavFile{

	private enum IOState {READING, WRITING, CLOSED};
	private final static int BUFFER_SIZE = 4096;

	private final static int FMT_CHUNK_ID = 0x20746D66;
	private final static int DATA_CHUNK_ID = 0x61746164;
	private final static int RIFF_CHUNK_ID = 0x46464952;
	private final static int RIFF_TYPE_ID = 0x45564157;

	private IOState ioState;				// Specifies the IO State of the Wav File (used for snaity checking)
	private int bytesPerSample;				// Number of bytes required to store a single sample
	private long numFrames;					// Number of frames within the data section
	private FileOutputStream oStream;		// Output stream used for writting data
	private FileInputStream iStream;		// Input stream used for reading data
	private double floatScale;				// Scaling factor used for int <-> float conversion				
	private double floatOffset;				// Offset factor used for int <-> float conversion				
	private boolean wordAlignAdjust;		// Specify if an extra byte at the end of the data chunk is required for word alignment

	// Wav Header
	private int numChannels;				// 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535)
	private long sampleRate;				// 4 bytes unsigned, 0x00000001 (1) to 0xFFFFFFFF (4,294,967,295)
													// Although a java int is 4 bytes, it is signed, so need to use a long
	private int blockAlign;					// 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535)
	private int validBits;					// 2 bytes unsigned, 0x0002 (2) to 0xFFFF (65,535)

	// Buffering
	private byte[] buffer;					// Local buffer used for IO
	private int bufferPointer;				// Points to the current position in local buffer
	private long frameCounter;				// Current number of frames read or written



	//CONSTRUCTOR//
	public WavFile(){
		buffer = new byte[BUFFER_SIZE];
	}



	//NEW WAV FILE//
	/*Create & init wav file*/
	public static WavFile newWavFile(File file, int numChannels, long numFrames, int validBits,
			long sampleRate) throws IOException, WavFileException{
		
		//1) Instantiate new Wavfile and initialise
		WavFile wavFile = new WavFile();
		wavFile.numChannels = numChannels;
		wavFile.numFrames = numFrames;
		wavFile.sampleRate = sampleRate;
		wavFile.bytesPerSample = (validBits + 7) / 8;
		wavFile.blockAlign = wavFile.bytesPerSample * numChannels;
		wavFile.validBits = validBits;

		//2) Sanity check arguments
		if(numChannels < 1 || numChannels > 65535) throw new WavFileException("Illegal number of channels, valid range 1 to 65536");
		if(numFrames < 0) throw new WavFileException("Number of frames must be positive");
		if(validBits < 2 || validBits > 65535) throw new WavFileException("Illegal number of valid bits, valid range 2 to 65536");
		if(sampleRate < 0) throw new WavFileException("Sample rate must be positive");

		//3) Create output stream for writing data
		wavFile.oStream = new FileOutputStream(file);

		//4) Calculate the chunk sizes
		long dataChunkSize = wavFile.blockAlign * numFrames;
		long mainChunkSize = 4 + 16 + 8 + dataChunkSize; //Riff Typ 8 + Format ID and size16 + Format data8 + Data Id size

		//5) Chunks must be word aligned, if odd number of audio data bytes adjust main chunk size
		if(dataChunkSize % 2 == 1){
			mainChunkSize += 1;
			wavFile.wordAlignAdjust = true;
		}
		else{
			wavFile.wordAlignAdjust = false;
		}

		//6) Set the main chunk size
		putLE(RIFF_CHUNK_ID, wavFile.buffer, 0, 4);
		putLE(mainChunkSize, wavFile.buffer, 4, 4);
		putLE(RIFF_TYPE_ID,	wavFile.buffer, 8, 4);

		//7) Write out the header
		wavFile.oStream.write(wavFile.buffer, 0, 12);

		//8) Put format data in buffer
		long averageBytesPerSecond = sampleRate * wavFile.blockAlign;
		putLE(FMT_CHUNK_ID, wavFile.buffer, 0, 4);		// Chunk ID
		putLE(16, wavFile.buffer, 4, 4);		// Chunk Data Size
		putLE(1, wavFile.buffer, 8, 2);		// Compression Code (Uncompressed)
		putLE(numChannels, wavFile.buffer, 10, 2);		// Number of channels
		putLE(sampleRate, wavFile.buffer, 12, 4);		// Sample Rate
		putLE(averageBytesPerSecond, wavFile.buffer, 16, 4);		// Average Bytes Per Second
		putLE(wavFile.blockAlign, wavFile.buffer, 20, 2);		// Block Align
		putLE(validBits, wavFile.buffer, 22, 2);		// Valid Bits

		//9) Write Format Chunk
		wavFile.oStream.write(wavFile.buffer, 0, 24);

		//10) Start Data Chunk
		putLE(DATA_CHUNK_ID, wavFile.buffer, 0, 4);		// Chunk ID
		putLE(dataChunkSize, wavFile.buffer, 4, 4);		// Chunk Data Size

		//11) Write Format Chunk
		wavFile.oStream.write(wavFile.buffer, 0, 8);

		//12) Calculate the scaling factor for converting to a normalised double
		if (wavFile.validBits > 8){
			// If more than 8 validBits, data is signed
			// Conversion required multiplying by magnitude of max positive value
			wavFile.floatOffset = 0;
			wavFile.floatScale = Long.MAX_VALUE >> (64 - wavFile.validBits);
		}
		else{
			// Else if 8 or less validBits, data is unsigned
			// Conversion required dividing by max positive value
			wavFile.floatOffset = 1;
			wavFile.floatScale = 0.5 * ((1 << wavFile.validBits) - 1);
		}

		//13) Set IO State
		wavFile.bufferPointer = 0;
		wavFile.frameCounter = 0;
		wavFile.ioState = IOState.WRITING;

		return wavFile;
	}



	//GET LE//
	/* Get little endian data from local buffer*/
	private static long getLE(byte[] buffer, int pos, int numBytes){
		numBytes --;
		pos += numBytes;

		long val = buffer[pos] & 0xFF;
		for (int b=0 ; b<numBytes ; b++)
			val = (val << 8) + (buffer[--pos] & 0xFF);

		return val;
	}
	//PUT LE//
	/*Put little endian data from local buffer*/
	private static void putLE(long val, byte[] buffer, int pos, int numBytes){
		for (int b=0 ; b<numBytes ; b++){
			buffer[pos] = (byte) (val & 0xFF);
			val >>= 8;
			pos ++;
		}
	}



	//WRITE SAMPLE//
	private void writeSample(long val) throws IOException{
		for (int b=0 ; b<bytesPerSample ; b++)
		{
			//1) Write
			if (bufferPointer == BUFFER_SIZE)
			{
				oStream.write(buffer, 0, BUFFER_SIZE);
				bufferPointer = 0;
			}

			//2) Next sample
			buffer[bufferPointer] = (byte) (val & 0xFF);
			val >>= 8;
			bufferPointer ++;
		}
	}



	//WRITE FRAMES//
	public double writeFrames(double[][] sampleBuffer, int offset, double numFramesToWrite) throws IOException, WavFileException{
		if (ioState != IOState.WRITING) throw new IOException("Cannot write to WavFile instance");

		for (int f=0 ; f<numFramesToWrite ; f++){//For each num frame to write in
			
			if (frameCounter == numFrames) 
				return f;

			for (int c=0 ; c<numChannels ; c++) 
				writeSample((long) (floatScale * (floatOffset + sampleBuffer[c][offset])));
			offset ++;
			frameCounter ++;
		}
		return numFramesToWrite;
	}



	//CLOSE FILE//
	public void close() throws IOException{
		
		//1) Close the input stream and set to null
		if (iStream != null){
			iStream.close();
			iStream = null;
		}
		if (oStream != null){
			//1.1) Write out anything still in the local buffer
			if (bufferPointer > 0) oStream.write(buffer, 0, bufferPointer);
			//1.2) If an extra byte is required for word alignment, add it to the end
			if (wordAlignAdjust) oStream.write(0);
			//1.3) Close the stream and set to null
			oStream.close();
			oStream = null;
		}
		
		//2) Flag that the stream is closed
		ioState = IOState.CLOSED;
	}




	//CREATE WAV//
	/*Create wav with data wanted*/
	public void createWav(String name, int sampleRate, double[] datas, int noteDuration){
		
    	try{
    		//1) Init frames
	        int sampleNbr = datas.length*noteDuration;
	        System.out.println("GOT : Total frames : " + sampleNbr);
	        
	        //2) Write in sample
	        WavFile wavFile = WavFile.newWavFile(new File(name), 1, sampleNbr, 16, sampleRate);
	        double[][] buffer = new double[1][sampleNbr];
	        for(int i=0; i<datas.length; i++){
	        	
	        	//2.1) Add tone
		        for (int t=0 ; t < noteDuration; t++)
		           buffer[0][t] = Math.sin(2.0*Math.PI*datas[i]*t/sampleRate);

		        //2.2) Write frame in wav
		        wavFile.writeFrames(buffer, 0, noteDuration);
		    }
	        wavFile.close();
    	}

    	catch(Exception e){
        	System.err.println(e);
    	}
    }



    //MAIN//
	public static void main(String[] args){
	}
	
	
	
	//GETTER//
	public int getNumChannels(){
		return numChannels;
	}

	public long getNumFrames(){
		return numFrames;
	}

	public long getFramesRemaining(){
		return numFrames - frameCounter;
	}
	
	public long getSampleRate(){
		return sampleRate;
	}

	public int getValidBits(){
		return validBits;
	}
}
