package fr.yncrea.whaleMusicGeneration.tools;

import fr.yncrea.whaleMusicGeneration.tools.complex.Complexe;
import fr.yncrea.whaleMusicGeneration.tools.complex.ComplexeCartesien;
import fr.yncrea.whaleMusicGeneration.tools.complex.ComplexePolaire;

public class FFT
{
	final static int FREQ_MIN = 100;
	final static int FREQ_MAX = 1000;
	final static int PRECISION_DIV = 1;

/*
 	private int taille;
 
	// Indiquer la taille dans le constructeur permettra des optimisations par la suite :
	// on pourra facilement transformer les methodes statiques en methodes
	// d'instance, et optimiser l'objet en fonction de la taille indiquee dans
	// le constructeur
 	public FFTCplx(int taille){
 		this.taille = taille;
 	}
*/



	//HALF SIGNAL//
	/*Extract only pairs or impairs values of a signal*/
	private static Complexe[] halfSignal(Complexe[] signal, int depart){
		Complexe[] sousSignal = new Complexe[signal.length/2];

		//get only pairs or impairs values (depending on depart)
		for(int i = 0; i < sousSignal.length; ++i)
			sousSignal[i] = signal[depart+2*i];

		return sousSignal;
	}



	//SET TO POWE 2//
	/*extends signal length into the next power of 2*/
	public static Complexe[] setToPower2(Complexe[] signal){
		Complexe[] P2signal;

		//1) Get P2signal length
		int goodValue = 1;
		do{
			if(signal.length <= goodValue)
				break;
			else
				goodValue *= 2;
		}while(signal.length != goodValue);

		//2) Init P2signal
		P2signal = new Complexe[goodValue];
		for(int i=0; i < goodValue; i++)
			P2signal[i] = new ComplexeCartesien(
				0,0
			);
		for(int i=0; i < signal.length; i++)
			P2signal[i] = new ComplexeCartesien(
				signal[i].reel(),
				signal[i].imag()
			);

		return P2signal;
	}



	//FFT COMPLEX//
	/*Fast Fourrier Transform on complex signal*/
	private static Complexe[] FFTComplex(Complexe[] signal){
		
		//1) set signal length in power of 2 (0 padding)
		signal = setToPower2(signal);

		//2) create new signal
		Complexe[] trSignal = new Complexe[signal.length];

		if(signal.length == 1)//trivial case
			trSignal[0] = new ComplexeCartesien(signal[0].reel(), signal[0].imag());
		else{
			//2.1) Cut in 2 sub signal
			final Complexe[] P0 = FFTComplex(halfSignal(signal, 0));
			final Complexe[] P1 = FFTComplex(halfSignal(signal, 1));

			//2.2) "butterfly" fusion
			for(int k=0; k < signal.length/2; ++k){
				//create trSignal[k]
				trSignal[k] = new ComplexeCartesien(0,0);

				//2.2.1) creating FFT components
				final ComplexePolaire expo = new ComplexePolaire(
					1.0,
					-2.0*Math.PI*k/signal.length
				);
				final Complexe temp = P0[k];

				//2.2.2) calculations
				trSignal[k] = temp.plus( expo.fois(P1[k]) );                  //temp + expo*P1[k]
				trSignal[k+signal.length/2] = temp.moins( expo.fois(P1[k]) ); //temp - expo*P1[k]
			}
		}

		return trSignal;
	}
	
	
	
	//FFT REAL//
	/*Fast Fourrier transform on real signal*/
	public float[] FFTReal(float[] signal){
		Complexe[] cplx = FFTComplex( floatsToCplxs(signal) );

		//fill with the same values
		float[] result = new float[cplx.length];
		for(int c=0; c < cplx.length; c++)
			result[c] = (float)cplx[c].mod();

		return result;
	}



	//FLOATS TO CPLXS//
	/*Convert a float signal to complex (only real part set)*/
	private static Complexe[] floatsToCplxs(float[] data){
		Complexe[] cplx = new Complexe[data.length];

		//set only real part
		for(int f=0; f < data.length; f++)
			cplx[f] = new ComplexeCartesien(
				(double)data[f],
				0
			);

		return cplx;
	}
}
