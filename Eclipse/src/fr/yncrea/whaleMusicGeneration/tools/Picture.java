package fr.yncrea.whaleMusicGeneration.tools;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Picture {

	//RESIZE//
	/*Resize an image*/
	public ImageIcon resize(String imgPath, int width, int height){
		
		//1) Get image
		ImageIcon imageIcon = new ImageIcon(imgPath);
		Image image = imageIcon.getImage();
		
		//2) Resize it
		Image resizeImg = image.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(resizeImg);

	    return imageIcon;
	}
	
}
