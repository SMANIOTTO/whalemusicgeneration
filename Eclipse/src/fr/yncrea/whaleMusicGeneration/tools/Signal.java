package fr.yncrea.whaleMusicGeneration.tools;

import fr.yncrea.whaleMusicGeneration.domain.Sound;

public class Signal {
	
	final public int sampleChunk = 220500;
	
	

	//METHODS//
	
	//SPLIT SIGNAL//
	/*cut signal to a specific length*/
	public float[][] split(float[] signal){
		float[][] result = new float[ (int)Math.ceil(signal.length/sampleChunk)+1 ][sampleChunk];

		//1) Init each chunk with signal values
		for(int s=0; s < result.length; s++){
			result[s] = new float[sampleChunk];

			//1.1) fill chunk
			for(int c=0; c < sampleChunk; c++){
				if(c+ s*sampleChunk < signal.length)//Signal size > chunk size
					result[s][c] = signal[c+ s*sampleChunk];
				else								//Chunck size < signal size
					result[s][c] = 0; //0 padding
			}
		}

		return result;
	}
	
	
	
	//GET FFT SIGNAL//
	/*Get fft sample signal part from path*/
	public float[][] getFFTSamplesSignal(int nbNeurons, int fftLength){
		float[][] outputFFT = new float[nbNeurons][fftLength];
		FFT fft = new FFT();
		
		System.out.println("TRY : Init default tones");
		
		//1) Create path, get datas & load fft on
		float[][] output_data = new float[nbNeurons][ sampleChunk ];
		String[] soundPath = new String[nbNeurons];
		String[] tones = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
		for(int i=0; i<nbNeurons; i++) {//For each sound path
			
			//1.1) Create path
			soundPath[i] = "resources/tones/" + tones[i] + ".wav";
			
			//1.2) Get datas
			output_data[i] = this.split(new Sound(soundPath[i]).getDatas())[0];
			
			//1.3) Load FFT for each output sound (useful for AI learning)
			outputFFT[i] = fft.FFTReal( output_data[i] );
		}
		
		System.out.println("INIT : Default tones\n");
		return outputFFT;
	}
	
	
	
    //GET AI OUTPUT DATAS//
	/* Get data from AI output and fill wav object */
    public double[] convertAIoutputToWAVdata(boolean[][] datas){
    	final double[] tone = {262, 277, 294, 311, 330, 349, 370, 392, 415, 440, 466, 494};

    	double[] wavDatas = new double[datas.length];
    	for(int i=0; i<datas.length; i++){
    		for(int k=0; k<datas[0].length; k++){
    			if(datas[i][k])
    				wavDatas[i] = tone[k];
    		}
    	}
    	return wavDatas;
    }
}
