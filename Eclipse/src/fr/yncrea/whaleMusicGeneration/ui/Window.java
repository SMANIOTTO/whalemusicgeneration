package fr.yncrea.whaleMusicGeneration.ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.yncrea.whaleMusicGeneration.domain.AI;
import fr.yncrea.whaleMusicGeneration.domain.Sound;
import fr.yncrea.whaleMusicGeneration.tools.FFT;
import fr.yncrea.whaleMusicGeneration.tools.Signal;
import fr.yncrea.whaleMusicGeneration.tools.wav.WavFile;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JButton;

public class Window extends JFrame {

	//WINDOW	
	private static JFrame frame = new JFrame();
	
	//MENUS
	private static JPanel[] menuPanels = new JPanel[5];
	private static int currentWindow = 0;
	
	private static Home home = new Home();
	private static Aquisition aquisition = new Aquisition();
	private static Settings settings = new Settings();
	private static Learning learning = new Learning();
	private static Generation generation = new Generation();
	
	//TOOLS
	private Signal signal = new Signal();


	//AI + FFT
	private AI ai;
	private FFT fft = new FFT();
	
	
	//CONSTRUCTOR//
	/*Construct btns, menus and window*/
	public Window(String title, int width, int height) {
		
		//1) Init btn
		JButton[] btns = new JButton[10];
		String[] btnsName = {"Launch", "Quit",			//HOME
				"Back", "Learn", "Generate", "Settings",//AQUISITION
				"Back",									//SETTINGS
				"Back",									//LEARNING
				"Back", "Reload"						//GENERATION
				};
		int[] btnXMin = {106, 393, 54, 254, 454, 10, 164, 190, 100, 400};
		int[] btnYMin = {21, 21, 21, 21, 21, 11, 11, 23, 21, 21};
		int[] btnWidth = {131, 131, 131, 131, 131, 131, 300, 241, 131, 131};
		for(int i=0; i<10; i++) {
			btns[i] = new JButton(btnsName[i]);								//CREATE
			btns[i].setBorder(BorderFactory.createLineBorder(Color.WHITE));	//BORDER
			btns[i].setFont(new Font("Arial", Font.PLAIN, 22));				//FONT
			btns[i].setBounds(btnXMin[i], btnYMin[i], btnWidth[i], 50);		//COORDS
			btns[i].setFocusable(false);									//FOCUS
			btns[i].setForeground(Color.WHITE);								//FONT COLOR
			btns[i].setBackground(Color.BLACK);								//BACKGROUND COLOR
			final int tmp = i;
			btns[i].addActionListener(new ActionListener() {				//ACTION
				public void actionPerformed(ActionEvent arg0) {
					
					int windowToLoad = 0;
					if(tmp == 0 || (tmp > 5 && tmp < 9)) {	//AQUISITION
						windowToLoad = 1;
					}
					else if(tmp == 1) {						//QUIT
						frame.setVisible(false);
						frame.dispose();
					}
					else if(tmp == 3) {						//LEARN
						windowToLoad = 3;
					}
					else if(tmp == 4 || tmp == 9) {			//GENERATION
						windowToLoad = 4;
					}
					else if(tmp == 5) {						//SETTINGS
						windowToLoad = 2;
					}
					else if(tmp == 2) {						//HOME
						windowToLoad = 0;
					}
					
					getMenuPane(windowToLoad);
				}
			});
		}
		
		//2) Init Menus
		menuPanels[0] = home.createPane(btns[0], btns[1]);							//HOME
		menuPanels[1] = aquisition.createPane(btns[2], btns[4], btns[3], btns[5]);	//AQUISITION
		menuPanels[2] = settings.createPane(btns[6]);								//SETTINGS
		menuPanels[3] = learning.createPane(12, btns[7]);							//LEARNING
		menuPanels[4] = generation.createPane(btns[8], btns[9]);					//GENERATION
		
		//3) Create window
		frame.setTitle(title);									//TITLE
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	//CLOSE
		frame.setBounds(100, 100, width, height);				//COORDS
		frame.setContentPane(menuPanels[0]);					//CONTENT
		frame.setVisible(true);									//DISPLAY
	}

		
	
	//GET MENU PANE//
	/*Set content of pane selected*/
	public void getMenuPane(int window) {
		
		//1) Clear pane content
		frame.remove(menuPanels[currentWindow]);
		currentWindow = window;
		
		//2) Set new content
		frame.setContentPane(menuPanels[window]);
		
		//3) Refresh
		frame.validate();
		frame.repaint();



		//4.1) Learn AI
		if(window == 3) {
	
			//1) Load utilities
			Sound defaultSound = new Sound("resources/tones/A.wav");
				
			//---------- INIT ----------//
			System.out.println("// INITIALISATION //");
			
			//2) Get default length
			int fftLength = fft.FFTReal(
				signal.split( defaultSound.getDatas() )[0]
			).length;
				
			//3) init AI
			ai = new AI(settings.getNbNeurons(), fftLength);
			
			//4) Start learning
			learning.start(settings.getNbNeurons(), fftLength, ai);
		}



		//4) Manage learn and generation
		if(aquisition.getSoundEntry() != null && ai != null && window == 4) {

				//4.2.1) Recongize tones
				System.out.println("\n//TONES RECOGNITION//\n");
				Sound originalSound = aquisition.getSoundEntry();
				float[] data = originalSound.getDatas(); //get all samples

				final int noteDuration = 10000;

				boolean[][] output = new boolean[data.length/noteDuration][settings.getNbNeurons()];
				for(int s=0; s < data.length/noteDuration; s++){
					for(int n=0; n < settings.getNbNeurons(); n++){
						//create chunk index s
						float[] chunk = new float[noteDuration];
						for(int c=0; c < noteDuration; c++)
							chunk[c] = data[s*noteDuration+c];

						//get AI result
						output[s][n] = ai.getNeurons()[n].isSame(
							fft.FFTReal( signal.split( chunk )[0] ), 1
						);
					}
				}

				//4.2.2) Generate wav sound
				System.out.println("\n//SOUND GENERATION//\n");
				
				double notesData[] = signal.convertAIoutputToWAVdata(output);
				//notesDate.length = originalSound.getDatas().length/noteDuration
				WavFile wavFile = new WavFile();
				
				wavFile.createWav(
					"resources/soundGenerated/" + settings.getFileName() + ".wav",
					originalSound.getFrequency(),
					notesData,
					noteDuration
				);

				generation.loadAudioFile(new File("resources/soundGenerated/" + settings.getFileName() + ".wav"));
		}
	}
}
