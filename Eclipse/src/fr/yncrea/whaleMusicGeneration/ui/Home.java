package fr.yncrea.whaleMusicGeneration.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import fr.yncrea.whaleMusicGeneration.tools.Picture;

import javax.swing.SwingConstants;
import javax.swing.JButton;

public class Home extends JFrame {
	
	//TOOLS
	private Picture tools = new Picture();
	
	
	
	//CREATE PANE//
	/*Create Home pane menu*/
	public JPanel createPane(JButton btnLaunch, JButton btnQuit) {
		
		//1) Create pane
		JPanel mainMenuPane = new JPanel();
		mainMenuPane.setBackground(new Color(34, 51, 59));
		mainMenuPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainMenuPane.setLayout(null);
		
		//2) Footer
		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon("resources/ui/footer.png"));
		footer.setBounds(0, 628, 1264, 53);
		mainMenuPane.add(footer);
		
		//3) Display
		JPanel display = new JPanel();
		display.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		display.setBackground(Color.BLACK);
		display.setBounds(666, 30, 567, 567);
		mainMenuPane.add(display);
		
		//3.1) Picture
		JLabel brain = new JLabel("");
		brain.setHorizontalAlignment(SwingConstants.CENTER);
		display.setLayout(new BorderLayout(0, 0));
		ImageIcon brainPic = tools.resize("resources/ui/brain.png", 400, 400);
		brain.setIcon(brainPic);
		display.add(brain);
		
		//4) Desc
		JPanel desc = new JPanel();
		desc.setBounds(29, 193, 640, 336);
		desc.setBackground(new Color(34, 51, 59));
		mainMenuPane.add(desc);
		desc.setLayout(new BorderLayout(0, 0));
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.WHITE);
		ImageIcon titlePic = tools.resize("resources/ui/title.png", 640, 450);
		lblNewLabel.setIcon(titlePic);
		desc.add(lblNewLabel, BorderLayout.CENTER);
		
		//5) Desc right
		JPanel descRight = new JPanel();
		descRight.setBounds(1231, 193, 23, 336);
		mainMenuPane.add(descRight);
		
		//6) Controls
		JPanel controls = new JPanel();
		controls.setBackground(new Color(34, 51, 59));
		controls.setBounds(29, 527, 640, 90);
		mainMenuPane.add(controls);
		controls.setLayout(null);
		
		//7) Btns
		controls.add(btnLaunch);
		controls.add(btnQuit);
		
		return mainMenuPane;
	}
}
